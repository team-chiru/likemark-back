FROM node:10
WORKDIR /usr/src/app

# Put likemark files into image
COPY . .

# Install likemark dependencies
RUN npm install --save https://github.com/mapbox/node-sqlite3/tarball/master \
    && npm install \
    && apt-get update \
    && apt-get install -y sqlite3 libsqlite3-dev \
    && touch src/database.db \
    && sqlite3 src/database.db < src/sql/LikemarkTable.sql

EXPOSE 8000
CMD [ "npm", "start" ]
