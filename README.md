# sidemark-back
likemark backend prototype.
## Contribute
### Installing and Script

```
npm install --save https://github.com/mapbox/node-sqlite3/tarball/master
```

```
npm install
```

Run the linter.

```
npm run lint
```

## Development
The start script start server on port 8000 that can be change in the file /.env root project directory.

```
npm start
```

## Initialise Likemark database
If you want to create the database, a `database.db` file must be initialized and a table must be created by the script: `LikemarkTable.sql`

The database must be create in the `/src` folder. So, run those commands to create the database:

```
cd src; sqlite3 database.db < sql/LikemarkTable.sql
```

## Unit tests
The unit test are run with Jest. More details in `package.json` file.

run this command to start unit test:
```
npm run test
```

## API documentation
### GET
Get a single likemark by id.
```
/likemark/get/:id
```

Get the first level of children of an likemark by id.
```
/likemark/getFirstChildren/:id
```

Get a single likemark by id with his first level of children.
```
/likemark/getWithFirstChildren/:id
```

List all likemarks in the database.
```
/likemark/list
```

### POST
Create a single likemark.
```
/likemark/post
```

Example create root:
```sh
curl -d '{"id":"root", "parentId": "root","title": "Folder231","url":"sidemark.io"}' -H "Content-Type: application/json" -X POST http://localhost:8000/likemark/post
```

### PATCH
Update a likemark by id.
```
/likemark/update/:id
```

### DELETE
Delete a likemark by id.
```
/likemark/delete/:id
```

### IMPORT
Import likemark from netscape file to database. (sync)
```
/likemark/import
```

Example:
```
curl -F file=@netscape.html http://localhost:8000/likemark/import
```

**Note:** It is synchronizing the likemarks with the netscape file. So, if you `import`, all previous likemark won't be there.


### EXPORT
Export likemark from database to netscape file.
```
/likemark/export
```

## Setup Docker

### Build
If you want to build a likemark-back image locally.
```sh
docker build -t likemark-back .
```

### Run
If you want to run a likemark-back container with an image:
```sh
docker run -d --name=likemark-back -p 8000:8000 $IMAGE_ID
```

### Bash
If you want to execute bash into the container. Use this command:
```
docker exec -ti $CONTAINER_ID bash
```
