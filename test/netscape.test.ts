import { serialize, deserialize } from 'serializr'
import * as Mustache from 'mustache'
import * as fs from 'fs'

import { Sequelize } from 'sequelize-typescript'
import { connection } from './database'
import { NetscapeAdapter } from '../src/logic/NetscapeAdapter'
import { IdMethod, IdFactory } from '../src/logic/IdFactory'
import { Likemark } from '../src/models/Likemark'
import { Row } from '../src/models/Row'
import { TreeManager } from '../src/logic/TreeManager'
import { Root } from '../src/models/Root'
import sequelize = require('sequelize')

const sample = deserialize(Root, {
  name: 'Likemarks',
  bookmarks: [{
    id: 'root',
    parentId: 'root',
    title: 'Folder0',
    url: null,
    children: [{
      id: '1',
      parentId: 'root',
      title: 'Folder1',
      url: null,
      children: [{
        id: '11',
        parentId: '1',
        title: 'Folder11',
        url: 'http://likemark.io'
      }, {
        id: '12',
        parentId: '1',
        title: 'Link12',
        url: 'http://likemark.io'
      }]
    }, {
      id: '2',
      parentId: 'root',
      title: 'Folder2',
      url: null,
      children: [{
        id: '21',
        parentId: '2',
        title: 'Folder21',
        url: 'http://likemark.io'
      }, {
        id: '22',
        parentId: '2',
        title: 'Folder22',
        url: 'http://likemark.io'
      }, {
        id: '23',
        parentId: '2',
        title: 'Folder23',
        url: null,
        children: [{
          id: '231',
          parentId: '23',
          title: 'Folder231',
          url: 'http://likemark.io'
        }]
      }]
    }, {
      id: '3',
      parentId: 'root',
      title: 'Link3',
      url: 'http://likemark.io'
    }]
  }]
})

// Test database connection and set server
beforeEach(async () => {
  try {
    await connection.authenticate()
    console.log('Connection has been established successfully.')
  } catch (error) {
    console.error('Unable to connect to the database:', error)
  }
})

afterEach(async () => {
  await Row.destroy({ where: {} })
  console.log('Connection has been successfully cleared.')
})

test('Test Tree Fetching', async () => {
  // expect empty db
  const empty = await Row.findAll<Row>()
  expect(empty).toEqual([])

  // create initial set
  await TreeManager.merge(sample)

  // fetch all the tree
  const root = await TreeManager.get()
  expect(root).toEqual(sample)
})

test('Test Export: Export a simple likemark object', async () => {
  const expected = fs.readFileSync('test/netscape.html', 'utf8')

  // expect empty db
  const empty = await Row.findAll<Row>()
  expect(empty).toEqual([])

  // create initial set
  await TreeManager.merge(sample)

  // export the initialized db
  const exported = await NetscapeAdapter.export()
  expect(exported).toBe(expected)
})

test('Test Import: Import a simple likemark object', async () => {
  const toImport = fs.readFileSync('test/netscape.html', 'utf8')
  const idFactory = new IdFactory(IdMethod.PARENT_ID)

  // expect empty db
  const empty = await Row.findAll<Row>()
  expect(empty).toEqual([])

  // import the initalized db
  await NetscapeAdapter.import(toImport, idFactory)

  // fetch all imported likemark
  const tree = await TreeManager.get()
  expect(tree).toEqual(sample)
})

test('Test duplicate import: Import twice the same file', async () => {
  const toImport = fs.readFileSync('test/netscape.html', 'utf8')
  const idFactory = new IdFactory(IdMethod.UUID)

  const empty = await Row.findAll<Row>()
  expect(empty).toEqual([])

  try {
    // import a netscape file twice
    await NetscapeAdapter.import(toImport, idFactory)
    await NetscapeAdapter.import(toImport, idFactory)
  } catch (error) {
    expect(error).toBeInstanceOf(sequelize.UniqueConstraintError)
  } finally {
    // the result is equivalent to only one import
    const all = await Row.findAll<Row>()
    expect(all.length).toBe(10)
  }
})
