DROP TABLE IF EXISTS Likemark;

CREATE TABLE IF NOT EXISTS Likemark (
    id TEXT NOT NULL PRIMARY KEY,
    parentId TEXT NOT NULL REFERENCES Likemark(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    title TEXT NOT NULL,
    url TEXT
);
INSERT INTO Likemark (id, parentId, title, url) VALUES ('0', '0', 'root', 'http://likemark.io' );
